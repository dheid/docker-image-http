FROM nginx:alpine

COPY default.conf /etc/nginx/conf.d/
COPY index.html images/* /usr/share/nginx/html/
