# HTTP Server with Sample Images (docker-image-http)

Provides an HTTP server that serves some example pictures in different formats to enable testing.

You can use this image for example in combination with Testcontainers to check download or processing capabilities
of your software.

This image uses the [Generic Docker Makefile](https://github.com/mvanholsteijn/docker-makefile) from
Mark van Holsteijn / Xebia to create the images and push them to the registry.

## Build

Just type

    make

to create a local image.

## Run

You can simply use Docker Compose to start the image

    docker-compose up -d

Now open

http://localhost

to see an HTML page presenting the images.

## Release

First login to Docker Hub using

    docker login

Please use the make target `major-release` to increase the release version, build it and push it to the registry.

    make [major,minor,patch]-release
    
